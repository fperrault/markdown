//----------------------------------------
//IMPORTS 
//----------------------------------------
import React from 'react'
import { render } from 'react-dom'

//CSS
import './style/css/bootstrap.min.css'
import './index.css'

//JS
import {sampleText} from './sampleText'

//Librairies externes
import marked from 'marked'


class App extends React.Component {

    //STATE
    state = {
        text: sampleText
    }

    //FUNCTIONS COMPONENTS
    componentWillUpdate(nextProps, nextState){
        localStorage.setItem('texteMarkdown', nextState.text)
    }

    componentWillMount(){
        const localStorageText = localStorage.getItem('texteMarkdown')

        if(localStorageText){
            this.setState({text: localStorageText})
        }
    }

    //PERSONNAL FUNCTION
    editText = (event) => {
        const text = event.target.value //valeur de sampleText après changement
        this.setState({text: text})
        this.setState({text}) //avec ES6 si mot=mot ({text: text})
    }

    renderText = (text) =>{
        const textRendu = marked(text, {sanitize: true})
        return {__html: textRendu} //bonne pratique
    }

    //RENDER
    render() {
        return (
            <div className="container">
                <div className="row" id="head">
                    <div className="col-sm-6">
                        <h1>Editeur</h1>
                    </div>
                    <div className="col-sm-6">
                        <h1>Résultat</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <textarea 
                            value={this.state.text} 
                            className="form-control" 
                            cols="30" 
                            rows="35"
                            onChange={this.editText}
                        >
                        </textarea>
                    </div>
                    <div className="col-sm-6">
                        <div dangerouslySetInnerHTML={this.renderText(this.state.text)}/>
                    </div>
                </div>
            </div>
        )
    }
}

render(
    <App/>,
    document.getElementById('root')
)